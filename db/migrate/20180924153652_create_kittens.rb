class CreateKittens < ActiveRecord::Migration[5.2]
  def change
    create_table :kittens do |t|
      t.string :name
      t.boolean :explodes
      t.integer :cuteness
      t.string :owner
      t.string :color

      t.timestamps
    end
  end
end
