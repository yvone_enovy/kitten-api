# Haz que tu server de rails responda 400 al crear un
# kitten si el owner no es ninguno de nosotros

class Kitten < ApplicationRecord
	#before_validation :owner_is_ok?, on: [ :create, :update ]

	def owner_is_ok?
		valid_owners = ["Yvone", "Kitty master", "John", "Peppa"]
		Rails.logger.info("******** self.inspect: #{self.inspect}")
		Rails.logger.info("******** self: #{self}")
		valid_owners.include? self.owner
	end

end
